﻿using System.Threading.Tasks;
using Advantage.DigitalNetwork.Security.Recaptcha;

namespace Advantage.DigitalNetwork.Tests.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
