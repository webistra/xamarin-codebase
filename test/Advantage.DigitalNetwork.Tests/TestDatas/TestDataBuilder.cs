﻿using Advantage.DigitalNetwork.EntityFrameworkCore;

namespace Advantage.DigitalNetwork.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly DigitalNetworkDbContext _context;
        private readonly int _tenantId;

        public TestDataBuilder(DigitalNetworkDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            new TestOrganizationUnitsBuilder(_context, _tenantId).Create();
            new TestSubscriptionPaymentBuilder(_context, _tenantId).Create();

            _context.SaveChanges();
        }
    }
}
