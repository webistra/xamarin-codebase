﻿using System.Threading.Tasks;
using Advantage.DigitalNetwork.ApiClient.Models;

namespace Advantage.DigitalNetwork.ApiClient
{
    public interface IAccessTokenManager
    {
        Task<string> GetAccessTokenAsync();
         
        Task<AbpAuthenticateResultModel> LoginAsync();

        void Logout();

        bool IsUserLoggedIn { get; }
    }
}