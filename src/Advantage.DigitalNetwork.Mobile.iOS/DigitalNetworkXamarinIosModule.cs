﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Advantage.DigitalNetwork
{
    [DependsOn(typeof(DigitalNetworkXamarinSharedModule))]
    public class DigitalNetworkXamarinIosModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DigitalNetworkXamarinIosModule).GetAssembly());
        }
    }
}