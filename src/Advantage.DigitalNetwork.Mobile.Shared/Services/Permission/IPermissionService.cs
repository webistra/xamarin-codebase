﻿namespace Advantage.DigitalNetwork.Services.Permission
{
    public interface IPermissionService
    {
        bool HasPermission(string key);
    }
}