﻿using Xamarin.Forms.Internals;

namespace Advantage.DigitalNetwork.Behaviors
{
    [Preserve(AllMembers = true)]
    public interface IAction
    {
        bool Execute(object sender, object parameter);
    }
}