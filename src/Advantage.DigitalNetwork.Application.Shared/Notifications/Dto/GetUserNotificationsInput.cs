﻿using Abp.Notifications;
using Advantage.DigitalNetwork.Dto;

namespace Advantage.DigitalNetwork.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}