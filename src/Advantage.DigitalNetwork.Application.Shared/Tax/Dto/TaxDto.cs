﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Advantage.DigitalNetwork.Tax.Dto
{
    public class TaxDto : EntityDto
    {
        public int Id { get; set; }
        public string TaxName { get; set; }
    }
}
