﻿using Abp.Application.Services;
using Advantage.DigitalNetwork.Tax.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Advantage.DigitalNetwork.Tax
{
    public interface ITaxService : IApplicationService
    {
        List<TaxCategoryDto> GetTaxCategories();
    }
}
