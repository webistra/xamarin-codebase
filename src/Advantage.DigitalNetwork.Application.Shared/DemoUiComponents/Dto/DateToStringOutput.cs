﻿namespace Advantage.DigitalNetwork.DemoUiComponents.Dto
{
    public class DateToStringOutput
    {
        public string DateString { get; set; }
    }
}
