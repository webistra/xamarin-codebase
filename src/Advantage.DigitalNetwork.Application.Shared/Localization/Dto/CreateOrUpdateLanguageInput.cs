﻿using System.ComponentModel.DataAnnotations;

namespace Advantage.DigitalNetwork.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}