using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Advantage.DigitalNetwork.Auditing.Dto;
using Advantage.DigitalNetwork.Dto;

namespace Advantage.DigitalNetwork.Auditing
{
    public interface IAuditLogAppService : IApplicationService
    {
        Task<PagedResultDto<AuditLogListDto>> GetAuditLogs(GetAuditLogsInput input);

        Task<FileDto> GetAuditLogsToExcel(GetAuditLogsInput input);
    }
}