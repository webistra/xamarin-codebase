﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Advantage.DigitalNetwork.Countries.Dto
{
    //[AutoMapFrom(typeof(AbpCountry))]
    public class CountryDto
    {
        //public DateTime CreationTime { get; set; }
        public int Id { get; set; }
        [Required]
        [StringLength(30)]
        public string Name { get; set; }
        [Required]
        [StringLength(30)]
        public bool AllowsBilling { get; set; }
        [Required]
        [StringLength(30)]
        public bool AllowsShipping { get; set; }
        [Required]
        [StringLength(30)]
        public string TwoLetterIsoCode { get; set; }
        [Required]
        [StringLength(30)]
        public string ThreeLetterIsoCode { get; set; }
        [Required]
        [StringLength(30)]
        public int NumericIsoCode { get; set; }
        [StringLength(30)]
        public bool SubjectToVat { get; set; }
        [StringLength(30)]
        public bool Published { get; set; }
        public int DisplayOrder { get; set; }
    }
}
