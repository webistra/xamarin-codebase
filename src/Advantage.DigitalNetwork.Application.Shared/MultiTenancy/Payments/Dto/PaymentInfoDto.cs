﻿using Advantage.DigitalNetwork.Editions.Dto;

namespace Advantage.DigitalNetwork.MultiTenancy.Payments.Dto
{
    public class PaymentInfoDto
    {
        public EditionSelectDto Edition { get; set; }

        public decimal AdditionalPrice { get; set; }
    }
}
