﻿namespace Advantage.DigitalNetwork.Sessions.Dto
{
    public class SubscriptionPaymentInfoDto
    {
        public decimal Amount { get; set; }
    }
}