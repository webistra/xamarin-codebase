﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Advantage.DigitalNetwork.Sessions.Dto;

namespace Advantage.DigitalNetwork.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
